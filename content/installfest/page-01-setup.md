Installfest
===========

## Requirements

In order to run Clojure and the tools we'll be using for
programming we need to install some free software on your laptop.
Most modern laptops should work just fine. Just to be sure
the requirements for each operating system are...

* **Mac OS X**<br/>
  Any Intel-based computer running Mac OS X 10.8 (Mountain Lion) or later.

* **Windows**<br/>
  Windows 7 or later

* **Linux**<br/>
  Any recent distribution that includes **openjdk-8-jdk** or installing
  [JDK 8 from Oracle](http://www.oracle.com/technetwork/java/javase/downloads/index.html) should be fine.

If you don't have access to a laptop or your laptop doesn't meet
the requirements above we have a few spare laptops that you can borrow.

By the end of these instructions, you will have the following installed:

* **Java** the platform atop which Clojure runs
* **Leiningen** a build tool for Clojure programs
* **Atom** a text editor
* **Git** a program for saving and sharing your program's code (included in the Heroku Toolbelt)
* The **Heroku** Toolbelt: a program for publishing your Clojure application on the web

And you will have:

* An account on **Slack** so you can chat with students and volunteers online
* Copied the example "Hello World"  application with ```git clone```
* Pushed your copy of the "Hello World"  application to Heroku so it's *live on the Internet*


## Instructions for all

These instructions apply to all platforms: Mac, Windows and Linux:

* [Slack](#slack)
* [Atom](#atom)
* [Github](#github)

## Instructions by operating system

Choose your operating system to get setup instructions:

* [Mac OS X](#os-x-setup)
* [Windows 7](#windows-7-setup)
* [Windows 8](#windows-8-setup)
* [Windows 10](#windows-10-setup)
* [Linux](#linux-setup)
