# Code of Conduct

Everyone should understand that participation in ClojureBridge
means agreeing to the Code of Conduct:

## Bridge Foundry Code of Conduct

We seek to create a community of software developers that are
reflective of the diversity in the general population. To that end, we
are dedicated to creating welcoming and friendly learning
environments.

All participants agree to the following:

* Everyone has something to contribute. Everyone deserves access to materials and community that will help them learn. As long as an individual can be respectful and not disruptive to other participants, they deserve to participate.
* If there is limited space and resources, anyone is welcome to replicate an event, such that more opportunities may be available. All curricula and organizing materials are open source and free. Provided that the organizer and event conform to project guidelines, we will help.
* We are dedicated to providing a harassment-free experience for everyone, regardless of gender, gender identity and expression, sexual orientation, disability, physical appearance, body size, race, religion, native language, operating system choice or prior experience. We do not tolerate harassment of participants in any form. Sexual language and imagery is not appropriate for any conference venue, including talks. Participants violating these rules may be sanctioned or expelled from an event or virtual forum at the discretion of the organizers.

Impact matters more than intent: If you didn’t realize that your behavior would have a negative impact that is your responsibility. Problems happen when we presume that our way of being is ok with everyone, when we assume that our way of thinking or behaving is the norm. This is particularly problematic when the other person is less empowered or is a member of a minority group.


For further information see [http://bridgefoundry.org/code-of-conduct/](http://bridgefoundry.org/code-of-conduct/)

## Whom to contact

Do you have Code of Conduct concerns, questions or actions you want to discuss?

Please contact Julie S. Mike at 651-269-3680 or juliesmike@gmail.com.
